import React from 'react'
import './carta.css'

interface props {
  handleClick: (index: number) => void;
  imagen: string;
  isClick: boolean;
  index: number;
}

export const Carta = ({ handleClick, imagen, isClick, index }: props) => {
  return (
    <div className={`carta ${!isClick && 'boca_abajo'} ${isClick && 'boca_arriba'}`} onClick={() => handleClick(index)}>
      {isClick ? <img src={imagen} /> : <span>?</span>}
    </div>
  )
}
