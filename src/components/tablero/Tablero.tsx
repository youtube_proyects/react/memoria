import { useCallback, useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { IMAGENES } from '../../assets/imagenes';
import { Carta } from '../carta/Carta';
import './tablero.css';


export const Tablero = () => {

    const [cartas, setCartas] = useState<any[]>();
    const [cartaSeleccionada1, setCartaSeleccionada1] = useState({ index: 0, img: '' });
    const [esperar, setEsperar] = useState(false);
    const [aciertos, setAciertos] = useState(0);
    const [reiniciar, setReiniciar] = useState(false);

    const cargaInicial = useCallback(() => {
        let cartas: any[] = [];

        for (let index = 0; index < 2; index++)
            for (const key in IMAGENES)
                if (Object.prototype.hasOwnProperty.call(IMAGENES, key))
                    cartas.push({ img: IMAGENES[key as keyof typeof IMAGENES], isClick: false });

        setCartas(cartas.sort(() => Math.random() - .5));
    }, [])

    useEffect(() => {
        cargaInicial();
    }, [reiniciar])

    const voltearCarta = (index: number) => {
        let data: any = JSON.parse(JSON.stringify(cartas));
        data[index]['isClick'] = true;
        setCartas(data);
        validar(index, data[index]['img']);
    }

    const validar = (index: number, img: string) => {
        let data: any;

        if (cartaSeleccionada1.img === '') {
            setCartaSeleccionada1({ ...cartaSeleccionada1, img: img, index: index })
            return;
        }

        if (cartaSeleccionada1.img === img) {
            setCartaSeleccionada1({ index: 0, img: '' });
            setAciertos(val => val + 1);

            if (aciertos === (cartas?.length! / 2) - 1) {
                Swal.fire({
                    title: 'Ganaste!!!!',
                    showClass: {
                        popup: 'animate__animated animate__fadeInDown'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp'
                    },
                    confirmButtonText: 'Volver a jugar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        setReiniciar(!reiniciar);
                        setAciertos(0);
                    }
                })
            }
            return
        } else {
            setEsperar(true);
            setTimeout(() => {
                setCartaSeleccionada1({ index: 0, img: '' });
                data = JSON.parse(JSON.stringify(cartas));
                data[cartaSeleccionada1.index]['isClick'] = false;
                data[index]['isClick'] = false;
                setCartas(data);
                setEsperar(false);
            }, 1000);
        }
    }

    return (
        <>
            {esperar && <div className='protector'></div>}
            <div className='tablero'>
                {cartas?.map((carta, index) =>
                    < Carta key={index} isClick={carta.isClick} handleClick={voltearCarta} index={index} imagen={carta.img} />
                )
                }
            </div>
        </>
    )
}
