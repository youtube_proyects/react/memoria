import './App.css';
import { Tablero } from './components/tablero/Tablero';

function App() {
  return (
    <div className="App">
      <Tablero />
    </div>
  );
}

export default App;
