import img1 from './imagenes/1.png';
import img2 from './imagenes/2.png';
import img3 from './imagenes/3.png';
import img4 from './imagenes/4.png';
import img5 from './imagenes/5.png';
import img6 from './imagenes/6.png';
import img7 from './imagenes/7.png';
import img8 from './imagenes/8.jpg';
import img9 from './imagenes/9.png';

export const IMAGENES = {
    img1,
    img2,
    img3,
    img4,
    img5,
    img6,
    img7,
    img8,
    img9
}